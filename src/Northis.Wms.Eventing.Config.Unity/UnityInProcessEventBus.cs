﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GreenPipes.Internals.Extensions;
using Microsoft.Practices.Unity;
using Northis.CQRS.Eventing;
using Northis.Wms.Eventing.Config.Unity.Properties;

namespace Northis.Wms.Eventing.Config.Unity
{
	/// <summary>
	/// Представляет реализацию <see cref="IEventBus"/> с обработкой событий на основе контейнера Unity в рамках процесса.
	/// </summary>
	public class UnityInProcessEventBus : IEventBus
	{
		#region Data

		#region Fields
		private readonly IUnityContainer _container;
		private readonly Logger _logger = LogManager.GetCurrentClassLogger();
		#endregion

		#endregion

		#region .ctor

		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="UnityInProcessEventBus"/>.
		/// </summary>
		/// <param name="container">Контейнер зависимостей.</param>
		public UnityInProcessEventBus(IUnityContainer container)
		{
			_container = container;
		}
		#endregion

		#region Public
		/// <summary>
		/// Публикует событие для обработки.
		/// </summary>
		/// <param name="event">Сообщение для публикации.</param>
		public void Publish(IEvent @event)
		{
			var handlers = FindHandlerForEvent(@event)
				.ToArray();
			if (!handlers.Any())
			{
				_logger.Warn(Resources.ErrorNotFoundEventHandlers, @event.ToString());
			}
			else
			{
				foreach (IEventHandler eventHandler in handlers)
				{
					_logger.Debug(Resources.Debug_InvokeEventHandler, eventHandler.ToString(),
								  @event.ToString());

					try
					{
						var handleMethod = eventHandler.GetType()
													   .GetMethod("Handle", new[]
																  {
																	  @event.GetType()
																  });
						handleMethod.Invoke(eventHandler, new object[]
											{
												@event
											});
					}
					catch(Exception exc)
					{
						_logger.Error(exc, Resources.Error_EventHandlerFailed, eventHandler.ToString(), @event.ToString());
					}
				}
			}
		}

		/// <summary>
		/// Публикует событие для обработки асинхронно.
		/// </summary>
		/// <param name="event">Событие для публикации.</param>
		/// <param name="token">Маркер отмены.</param>
		/// <returns>Асинхронная операция.</returns>
		public Task PublishAsync(IEvent @event, CancellationToken token = new CancellationToken())
		{
			return Task.Run(() =>
								{
									Publish(@event);
								}, token);
		}
		#endregion

		#region Private
		private IEnumerable<IEventHandler> FindHandlerForEvent<TPayload>(TPayload @event)
		{
			var eventType = @event.GetType();
			var interfaceType = typeof(IEventHandler<>).MakeGenericType(eventType);

			var handlerTypes = _container.Registrations.Where(registration => registration.MappedToType.HasInterface(typeof(IEventHandler)))
										 .Select(registration => registration.MappedToType);

			var handlers = new List<IEventHandler>();
			foreach (var handlerType in handlerTypes)
			{
				if (handlerType.HasInterface(interfaceType))
				{
					handlers.Add((IEventHandler)_container.Resolve(handlerType));
				}
			}

			return handlers;
		}
		#endregion
	}
}

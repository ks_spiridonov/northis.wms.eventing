﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.Unity;

namespace Northis.Wms.Eventing.Config.Unity
{
	/// <summary>
	/// Представляет регистрацию зависимостей для обработки событий.
	/// </summary>
	/// <seealso cref="Microsoft.Practices.Unity.UnityContainerExtension" />
	public class EventsUnityExtension : UnityContainerExtension
	{
		#region Data

		#region Fields
		private readonly IEnumerable<Assembly> _assemblies;
		#endregion

		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="EventsUnityExtension"/>.
		/// </summary>
		/// <param name="assemblies">Сборки.</param>
		public EventsUnityExtension(IEnumerable<Assembly> assemblies)
		{
			_assemblies = assemblies;
		}
		#endregion

		#region Overrided
		/// <summary>
		/// Initial the container with this extension's functionality.
		/// </summary>
		/// <remarks>
		/// When overridden in a derived class, this method will modify the given
		/// <see cref="T:Microsoft.Practices.Unity.ExtensionContext" /> by adding strategies, policies, etc. to
		/// install it's functions into the container.
		/// </remarks>
		protected override void Initialize()
		{
			var availableTypes = AllClasses.FromAssemblies(_assemblies)
										   .ToArray();

			Func<Type, bool> isImplement =
				type =>
					type.GetInterfaces()
						.Any(iface => iface.IsGenericType && iface.GetGenericTypeDefinition() == typeof(IEventHandler<>));

			var handlerTypes = availableTypes
				.Where(isImplement)
				.ToArray();
			foreach (var handlerType in handlerTypes)
			{
				Container.RegisterType(handlerType);
			}
		}
		#endregion
	}
}

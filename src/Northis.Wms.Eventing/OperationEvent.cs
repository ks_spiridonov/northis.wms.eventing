﻿using System;
using System.ComponentModel;
using System.Reflection;
using Newtonsoft.Json;
using Northis.CQRS.Eventing;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет событие, связанное с завершенной операцией на складе.
	/// </summary>
	/// <seealso cref="Northis.CQRS.Eventing.Event" />
	/// <seealso cref="Northis.Wms.Eventing.IOperationEvent" />
	[Serializable]
	public abstract class OperationEvent : Event, IOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="OperationEvent" />.
		/// </summary>
		protected OperationEvent()
			: base(Guid.NewGuid(), DateTime.Now, null)
		{
		}
		#endregion

		#region IOperationEvent members
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		[JsonIgnore]
		public virtual string Summary
		{
			get
			{
				return string.Format(Resources.StrOperationEventDescription, OperationDisplayName);
			}
		}

		/// <summary>
		/// Возвращает отображаемое имя операции, связанной с событием.
		/// </summary>
		/// <value>Отображаемое имя операции.</value>
		public string OperationDisplayName
		{
			get
			{
				return GetDisplayName();
			}
		}
		#endregion

		#region Private
		private string GetDisplayName()
		{
			// TODO добавить возможность локализованной строки

			var type = GetType();
			var attribute = type.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
			if (attribute != null)
			{
				return attribute.DisplayName;
			}

			return type.Name;
		}

		/// <summary>
		/// Возвращает или устанавливает объект, который является инициатором события.
		/// </summary>
		/// <value>
		/// Объект-инициатор события.
		/// </value>
		/// <remarks>
		/// Авторизованный пользователь системы, приложение.
		/// </remarks>
		public EventPrincipal Principal
		{
			get;
			set;
		}
		#endregion
	}
}

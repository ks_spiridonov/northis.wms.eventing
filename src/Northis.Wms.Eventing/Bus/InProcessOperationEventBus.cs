﻿using System.Collections.Generic;
using System.Linq;

namespace Northis.Wms.Eventing.Bus
{
	/// <summary>
	/// Представляет шину сообщений от системы слежения.
	/// </summary>
	public class InProcessOperationEventBus : IOperationEventBus
	{
		#region Data

		#region Fields
		private readonly IList<IOperationEvent> _events = new List<IOperationEvent>();
		#endregion

		#endregion

		#region Properties
		/// <summary>
		/// Возвращает публикуемые события.
		/// </summary>
		/// <value>Перечисление событий.</value>
		public IReadOnlyCollection<IOperationEvent> Events => _events.ToList();
		#endregion

		#region IOperationEventBus members
		/// <summary>
		/// Публикует в шину сообщение.
		/// </summary>
		/// <param name="event">публикуемое сообщение.</param>
		/// <seealso cref="IOperationEvent" />
		public void Publish(IOperationEvent @event)
		{
			_events.Add(@event);
		}
		#endregion

		#region IEventPublisher<IOperationEvent> members
		/// <summary>
		/// Очищает список публикуемых событий <see cref="Events" />
		/// </summary>
		public void Clear()
		{
			_events.Clear();
		}
		#endregion
	}
}

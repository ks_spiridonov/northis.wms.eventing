﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using Northis.Wms.Common.Enums;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее, когда происходит смена ГЗМ.
	/// </summary>
	/// <seealso cref="DeviceEvent" />
	[Serializable]
	[DisplayName("Смена ГЗМ")]
	public class CraneWorkMechanismChangedEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		/// <param name="oldWorkMechanismData">Старый ГЗМ.</param>
		/// <param name="newWorkMechanismData">Новый ГЗМ.</param>
		public CraneWorkMechanismChangedEvent(WarehouseObjectFlatDto deviceData,
			WarehouseObjectFlatDto oldWorkMechanismData,
			WarehouseObjectFlatDto newWorkMechanismData)
			: base(deviceData)
		{
			Contract.Requires<ArgumentException>(deviceData.Type == WarehouseObjectType.Crane);
			OldWorkMechanismData = oldWorkMechanismData;
			NewWorkMechanismData = newWorkMechanismData;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает данные по новому ГЗМ.
		/// </summary>
		/// <value>
		/// Данные по новому ГЗМ.
		/// </value>
		public WarehouseObjectFlatDto NewWorkMechanismData
		{
			get;
		}

		/// <summary>
		/// Возвращает данные по старому ГЗМ.
		/// </summary>
		/// <value>
		/// Данные по старому ГЗМ.
		/// </value>
		public WarehouseObjectFlatDto OldWorkMechanismData
		{
			get;
		}
		#endregion

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				var oldDataString = OldWorkMechanismData?.ToString() ?? Resources.StrNotDefined;
				var newDataString = NewWorkMechanismData?.ToString() ?? Resources.StrNotDefined;
				return string.Format(Resources.StrWorkMechanismChanged, oldDataString, newDataString);
			}
		}
	}
}

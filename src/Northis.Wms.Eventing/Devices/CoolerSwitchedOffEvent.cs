﻿using System;
using System.ComponentModel;

namespace Northis.Wms.Eventing.Devices
{
	/// <summary>
	/// Представляет событие, возникающее, когда УУОР выключается
	/// </summary>
	/// <seealso cref="DeviceEvent" />
	[Serializable]
	[DisplayName("УУОР выключен")]
	public class CoolerSwitchedOffEvent : DeviceEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="DeviceEvent" />.
		/// </summary>
		/// <param name="deviceData">Данные по устройству.</param>
		public CoolerSwitchedOffEvent(WarehouseObjectFlatDto deviceData)
			: base(deviceData)
		{
		}
		#endregion
	}
}

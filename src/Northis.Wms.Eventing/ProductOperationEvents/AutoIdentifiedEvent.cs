﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда продукция идентфицирована автоматически.
	/// </summary>
	[Serializable]
	[DisplayName("Автоматическая идентификация")]
	public class AutoIdentifiedEvent : ProductIdentifiedEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="mergingProductIds">Список идентификаторов продукции которые были объединены.</param>
		public AutoIdentifiedEvent(WarehouseObjectFlatDto productData,
			WarehouseObjectFlatDto productOwnerData,
			IEnumerable<Guid> mergingProductIds)
			: base(productData, productOwnerData, mergingProductIds)
		{
		}
		#endregion
	}
}

﻿using System;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда продукция переведена в потерянные.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Переход в список потерянных")]
	public class ProductLostEvent : ProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		public ProductLostEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData)
			: base(productData, productOwnerData)
		{
			
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrProductLostEventDescription, ProductData);
			}
		}
		#endregion
	}
}

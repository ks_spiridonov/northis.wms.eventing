﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using Northis.Wms.Eventing.Properties;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда продукция идентифицирована.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Идентификация")]
	public class ProductIdentifiedEvent : ProductOperationEvent, IProductMergingEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="mergingProductIds">Список идентификаторов продукции которые были объединены.</param>
		public ProductIdentifiedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData,
			IEnumerable<Guid> mergingProductIds)
			: base(productData, productOwnerData)
		{
			Contract.Requires<ArgumentNullException>(mergingProductIds != null);
			Contract.Requires<ArgumentException>(Contract.ForAll(mergingProductIds, id => id != default(Guid)));
			MergeResultProductId = productData.Id;
			MergingProductIds = mergingProductIds;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает идентификатор продукта полученого в процессе объединения.
		/// </summary>
		public Guid MergeResultProductId
		{
			get;
			private set;
		}

		/// <summary>
		/// Возвращает список продуктов которые были объединены в <see cref="MergeResultProductId" />.
		/// </summary>
		public IEnumerable<Guid> MergingProductIds
		{
			get;
			private set;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>
		/// Описание.
		/// </value>
		public override string Summary => string.Format(Resources.ProductIdentifiedEvent_Description, ProductData);
		#endregion
	}
}

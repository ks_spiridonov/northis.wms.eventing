﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Northis.Wms.Eventing.Properties;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие возникающее при переидентификации рулона.
	/// </summary>
	/// <seealso cref="Northis.Wms.Eventing.ProductOperationEvents.ProductIdentifiedEvent" />
	[Serializable]
	[DisplayName("Переидентификация")]
	public class ProductReidentifiedEvent : ProductIdentifiedEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductReidentifiedEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="mergingProductIds">Список идентификаторов продукции которые были объединены.</param>
		/// <param name="reason">Причина переидентификации.</param>
		public ProductReidentifiedEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData, IEnumerable<Guid> mergingProductIds,
			string reason) : base(productData, productOwnerData, mergingProductIds)
		{
			Reason = reason;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает причину переидентификации.
		/// </summary>
		public string Reason
		{
			private set;
			get;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>
		/// Описание.
		/// </value>
		public override string Summary => string.Format(Resources.ProductReidentifiedEvent_Description, ProductData, Reason);
		#endregion
	}
}

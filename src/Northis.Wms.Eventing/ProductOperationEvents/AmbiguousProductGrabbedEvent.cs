﻿using System;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Text;
using Newtonsoft.Json;
using Northis.Wms.Eventing.Properties;

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет событие, возникающее, когда одна из ед. продукции захвачена краном из нескольких возможных, требуется
	/// подтверждения крановщика.
	/// </summary>
	/// <seealso cref="ProductOperationEvent" />
	[Serializable]
	[DisplayName("Поднятие краном с необходимостью подтверждения")]
	public class AmbiguousProductGrabbedEvent : ProductOperationEvent, IDeviceOperationEvent
	{
		#region Data

		#region Fields
		[JsonProperty("AmbiguousProducts")]
		private readonly WarehouseObjectFlatDto[] _ambiguousProducts;
		#endregion

		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="AmbiguousProductGrabbedEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		/// <param name="deviceData">Данные крана в рамках операции.</param>
		/// <param name="isManual">Если установлено <c>true</c>, операция выполнена в ручном режиме.</param>
		/// <param name="ambiguousProducts">Возможные кандидаты для захвата краном.</param>
		public AmbiguousProductGrabbedEvent(WarehouseObjectFlatDto productData,
			WarehouseObjectFlatDto productOwnerData,
			WarehouseObjectFlatDto deviceData,
			WarehouseObjectFlatDto[] ambiguousProducts,
			bool isManual = false)
			: base(productData, productOwnerData)
		{
			Contract.Requires<ArgumentNullException>(deviceData != null);
			Contract.Requires<ArgumentNullException>(ambiguousProducts != null);
			Contract.Requires<ArgumentException>(Contract.ForAll(ambiguousProducts, product => product != null));
			_ambiguousProducts = ambiguousProducts;
			DeviceData = deviceData;
			IsManual = isManual;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает значение, указывающее, что операция выполнена в ручном режиме.
		/// </summary>
		/// <value><c>true</c> если операция выполнена в ручном режиме; иначе, <c>false</c>.</value>
		public bool IsManual
		{
			get;
		}
		#endregion

		#region IDeviceOperationEvent members
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные о кране.</value>
		public WarehouseObjectFlatDto DeviceData
		{
			get;
		}
		#endregion

		#region IOperationEvent members
		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrAmbiguousProductGrabbed, ProductData, DeviceData, ProductOwnerData,
									 IsManual ? Resources.StrManualMode : Resources.StrAutomaticMode, GetProductsInfo());
			}
		}
		#endregion

		#region Private
		private string GetProductsInfo()
		{
			var builder = new StringBuilder();
			foreach (var warehouseObjectFlatDto in _ambiguousProducts)
			{
				builder.AppendFormat("{0}; ", warehouseObjectFlatDto);
			}

			return builder.ToString()
						  .Trim()
						  .Trim(';');
		}
		#endregion
	}
}

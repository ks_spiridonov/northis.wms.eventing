﻿using System;
using System.Diagnostics.Contracts;
using Northis.Wms.Eventing.Properties;

// ReSharper disable AutoPropertyCanBeMadeGetOnly.Local

namespace Northis.Wms.Eventing.ProductOperationEvents
{
	/// <summary>
	/// Представляет базовый тип события, связанный с операцией над продукцией.
	/// </summary>
	/// <seealso cref="IProductOperationEvent" />
	[Serializable]
	public abstract class ProductOperationEvent : OperationEvent, IProductOperationEvent
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ProductOperationEvent" />.
		/// </summary>
		/// <param name="productData">Данные единицы продукции в рамках операции.</param>
		/// <param name="productOwnerData">Данные объекта, которому принадлежит продукция в рамках операции.</param>
		protected ProductOperationEvent(WarehouseObjectFlatDto productData, WarehouseObjectFlatDto productOwnerData)
		{
			Contract.Requires<ArgumentNullException>(productData != null);
			Contract.Requires<ArgumentNullException>(productOwnerData != null);
			ProductData = productData;
			ProductOwnerData = productOwnerData;
		}
		#endregion

		#region Properties
		
		/// <summary>
		/// Возвращает данные единицы продукции в рамках операции.
		/// </summary>
		/// <value>Данные единицы продукции.</value>
		public WarehouseObjectFlatDto ProductData
		{
			get;
			protected set;
		}

		/// <summary>
		/// Возвращает данные объекта, которому принадлежит продукция в рамках операции.
		/// </summary>
		/// <value>Данные объекта, которому принадлежит продукция.</value>
		/// <remarks>
		/// Представляет конечный объект в структуре склада, в котором находится продукция в рамках операции (пролет,
		/// зона, устройство, место постановки и т.п.). Исключение: едница хранения.
		/// </remarks>
		public WarehouseObjectFlatDto ProductOwnerData
		{
			get;
			protected set;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		public override string Summary
		{
			get
			{
				return string.Format(Resources.StrProductOperationEventDescription, ProductData, OperationDisplayName);
			}
		}
		#endregion
	}
}

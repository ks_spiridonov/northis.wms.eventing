﻿namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет событие на операцию, связанную с устройстом.
	/// </summary>
	public interface IDeviceOperationEvent : IOperationEvent
	{
		/// <summary>
		/// Возвращает данные об устройстве в рамках операции.
		/// </summary>
		/// <value>Данные об устройстве.</value>
		WarehouseObjectFlatDto DeviceData
		{
			get;
		}
	}
}
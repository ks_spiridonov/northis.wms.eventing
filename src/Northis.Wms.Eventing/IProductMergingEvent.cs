﻿using System;
using System.Collections.Generic;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Предоставляет события при объединении продуктов.
	/// </summary>
	public interface IProductMergingEvent : IOperationEvent
	{
		#region Properties
		/// <summary>
		/// Возвращает идентификатор продукта, полученого в процессе объединения.
		/// </summary>
		Guid MergeResultProductId
		{
			get;
		}

		/// <summary>
		/// Возвращает список идентификаторов продуктов, которые были объединены в <see cref="MergeResultProductId" />.
		/// </summary>
		IEnumerable<Guid> MergingProductIds
		{
			get;
		}
		#endregion
	}
}

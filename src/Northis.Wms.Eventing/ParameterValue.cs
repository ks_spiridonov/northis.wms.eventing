﻿using System;
using System.Linq.Expressions;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет информацию по значению параметра объекта в событии.
	/// </summary>
	[Serializable]
	public class ParameterValue
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ParameterValue" />.
		/// </summary>
		/// <param name="expression">Expression для получения исходного значения.</param>
		public ParameterValue(Expression<Func<object>> expression)
		{
			SetValue(expression);
		}

		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ParameterValue" />.
		/// </summary>
		/// <param name="nativeValue">Исходное значение параметра.</param>
		/// <param name="uiDisplayName">Отображаемое имя параметра.</param>
		public ParameterValue(object nativeValue, string uiDisplayName)
		{
			NativeValue = nativeValue;
			UiDisplayName = uiDisplayName;
		}

		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="ParameterValue" />.
		/// </summary>
		private ParameterValue()
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Возвращает или устанавливает значение, указывающее, хранится ли параметр во внешнем источнике.
		/// </summary>
		/// <value><c>true</c> если параметр хранится во внешнем источнике; иначе, <c>false</c>.</value>
		public bool IsStored
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает исходное значение параметра.
		/// </summary>
		/// <value>Значение параметра.</value>
		public object NativeValue
		{
			get;
			private set;
		}

		/// <summary>
		/// Возвращает отображаемое имя параметра.
		/// </summary>
		/// <value>Отображаемое имя параметра.</value>
		public string UiDisplayName
		{
			get;
			private set;
		}
		#endregion

		#region Overrided
		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return $"{UiDisplayName} : {NativeValue}";
		}
		#endregion

		#region Private
		private void SetValue(Expression<Func<object>> expression)
		{
			var pair = ExpressionHelper.GetDisplayValue(expression);
			UiDisplayName = pair.Item1;
			NativeValue = pair.Item2;
		}
		#endregion
	}
}

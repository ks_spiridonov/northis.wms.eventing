﻿using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет событие на операцию системы слежения.
	/// </summary>
	/// <seealso cref="Northis.CQRS.Eventing.IEvent" />
	public interface IOperationEvent : IEvent
	{
		/// <summary>
		/// Возвращает описание операции, связанной с событием.
		/// </summary>
		/// <value>Тип операции.</value>
		string OperationDisplayName
		{
			get;
		}

		/// <summary>
		/// Возвращает текстовое описание события.
		/// </summary>
		/// <value>Описание.</value>
		string Summary
		{
			get;
		}

		/// <summary>
		/// Возвращает или устанавливает объект, который является инициатором события.
		/// </summary>
		/// <value>
		/// Объект-инициатор события.
		/// </value>
		/// <remarks>Авторизованный пользователь системы, приложение.</remarks>
		EventPrincipal Principal
		{
			get;
			set;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Northis.Common.Extensions;
using Northis.Wms.Common.Enums;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет dto-объект для передачи данных об объектах в событиях.
	/// </summary>
	[Serializable]
	public class WarehouseObjectFlatDto
	{
		#region .ctor

		/// <summary>
		/// Инициализирует новый экземпляр типа <see cref="WarehouseObjectFlatDto" />.
		/// </summary>
		public WarehouseObjectFlatDto(Guid id, WarehouseObjectType type)
		{
			Id = id;
			Parameters = new Dictionary<string, ParameterValue>();
			Type = type;
		}

		#endregion

		#region Properties
		/// <summary>
		/// Возвращает или устанавливает идентификатор.
		/// </summary>
		public Guid Id
		{
			get;
			set;
		}

		/// <summary>
		/// Возвращает словарь с данными объекта.
		/// </summary>
		public IDictionary<string, ParameterValue> Parameters
		{
			get;
		}

		/// <summary>
		/// Возвращает хранимый тип объекта.
		/// </summary>
		public WarehouseObjectType Type
		{
			get;
		}
		#endregion

		#region Protected
		protected bool Equals(WarehouseObjectFlatDto other)
		{
			return Id.Equals(other.Id);
		}

		#endregion

		#region Overrided

		/// <summary>Determines whether the specified object is equal to the current object.</summary>
		/// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
		/// <param name="obj">The object to compare with the current object. </param>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			if (ReferenceEquals(this, obj))
				return true;
			if (obj.GetType() != GetType())
				return false;

			return Equals((WarehouseObjectFlatDto)obj);
		}

		/// <summary>Serves as the default hash function. </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			ParameterValue value;
			var res = Parameters.TryGetValue(WarehouseObjectKeys.DisplayName, out value);
			if (res)
			{
				return $"{Type.GetDescription()} : {Parameters[WarehouseObjectKeys.DisplayName].NativeValue}";
			}

			return $"{Type.GetDescription()}";
		}

		#endregion
	}
}

﻿using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет интерфейс опубликованного события.
	/// </summary>
	/// <seealso cref="Northis.CQRS.Eventing.IEvent" />
	public interface IPublishedEvent : IEvent
	{
		/// <summary>
		/// Возвращает бизнес-детали события (полезные детали).
		/// </summary>
		/// <value>Бизнес-детали события.</value>
		object Payload
		{
			get;
		}
	}
}
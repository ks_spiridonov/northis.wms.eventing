﻿using System;
using Northis.CQRS.Eventing;

namespace Northis.Wms.Eventing
{
	/// <summary>
	/// Представляет опубликованное событие.
	/// </summary>
	/// <seealso cref="IPublishedEvent" />
	[Serializable]
	public class PublishedEvent : IPublishedEvent
	{
		#region .ctor
		/// <summary>Initializes a new instance of the <see cref="T:System.Object" /> class.</summary>
		public PublishedEvent(IEvent @event)
		{
			EventIdentifier = @event.EventIdentifier;
			EventTimeStamp = @event.EventTimeStamp;
			EventVersion = @event.EventVersion;
			Payload = @event;
		}
		#endregion

		#region IEvent members
		/// <summary>Идентефикатор</summary>
		/// <value>The event identifier.</value>
		public Guid EventIdentifier
		{
			get;
		}

		/// <summary>Gets the event time stamp.</summary>
		/// <value>The event time stamp.</value>
		public DateTime EventTimeStamp
		{
			get;
		}

		/// <summary>Gets the event version.</summary>
		/// <value>The event version.</value>
		public Version EventVersion
		{
			get;
		}
		#endregion

		#region IPublishedEvent members
		/// <summary>
		/// Возвращает бизнес-детали события (полезные детали).
		/// </summary>
		/// <value>Бизнес-детали события.</value>
		public object Payload
		{
			get;
		}
		#endregion
	}
}
